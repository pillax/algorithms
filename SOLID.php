<?php
// SOLID 

//////////////////////////////////////////////////////
// Single Responsibility Principle
// 1 class should has ONLY 1 reason to change

//////////////////////////////////////////////////////
// Open / Closed Principle
// Every class is opened for extension, but closed for modifications 
class badOpenClosed {
    public static function convert(array $arr, string $type) : string {
        if($type == 'json') {
            return json_encode($arr);
        } elseif ($type == 'serialized') {
            return serialize($arr);
        }
        // ...
    }
}

class goodOpenClosed {
    public static function convert(array $arr, ConverterDriver $Driver) : string {
        return $Driver->process($arr);
    }
}

class ConverterJson implements iConverter {
    public function process(array $arr) {
        return json_encode($arr);
    }
}

//////////////////////////////////////////////////////
// Liskov Substitution Principle
// Пинцип на заменяемост
// както се използва родителя, така да се използва и наследника. 
// наследниците да допълват но не да променят родителя


//////////////////////////////////////////////////////
// Interface Segregation Principle
// мн интерфейси вместо един.

//////////////////////////////////////////////////////
// Dependency Inversion Principle
// DI
// вместо хардкодната зависимост
class badClass {
    public function __construct() {
        $this->processor = new PayPallProcessor();
    }
}

class goodClass {
    public function __construct(iProcessor $processor) {
        $this->processor = $processor;
    }
}




