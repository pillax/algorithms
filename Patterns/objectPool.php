<?php

class ObjPool {
    private $bussy = [];
    private $free = [];

    public function getWorker() : iWorker {
        if($this->free) {
            $worker = array_pop($this->free);
        } else {
            $worker = new MyClass();
        }

        $this->bussy[$worker->getObjId()] = $worker;

        return $worker;
    }

    public function releaseWorker(iWorker $worker) {
        $this->free[] = $worker;
    }
}

interface iWorker {
    public function getObjId() : string;
}

class MyClass  implements iWorker {
    private $myId;

    public function getObjId(): string {
        
        if( ! $this->myId) {
            $this->myId = (string) microtime() . rand(1000, 9999);
        }

        return $this->myId ;
    }
}
