<?php 

class Event {
    const EVENT_LOGIN = 1;
    const EVENT_LOGOUT = 2;

    private static $events = [];

    public static function listen(int $event, Closure $callBack) : void {
        self::$events[$event][] = $callBack;
    }

    public static function trigger(int $event, array $args = []) : void {
        foreach(self::$events[$event] AS $closure) {
            call_user_func_array($closure, $args);
        }
    }
}

Event::listen(Event::EVENT_LOGIN, function() {
    print 'Opala';
});

Event::trigger(Event::EVENT_LOGIN, ['username' => $username]);