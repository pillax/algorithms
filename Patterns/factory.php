<?php
////////////////////////////////////////////////////////////
// Simple Factory
// Има 1 метод, връща обект принадлежащ на 1 интерфейс
// iOSClass

interface OSDepended{}
class WinClass implements OSDepended{}
class LinClass implements OSDepended {}
class SimpleFactory {
    public static function create() : OSDepended {
        if(config('OS') == 'win') {
            return new WinClass();
        } else if(config('OS') == 'lin') {
            return new LinClass();
        }

        throw new Exception('Unknown OS');
    }
}

////////////////////////////////////////////////////////////
// Abstract factory
// Връща обект който може да принадлежи на разл интерфейси
class AbstractFactory {

}
