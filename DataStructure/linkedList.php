<?php
// Linked list

class Node 
{
    private $data;
    private $next;

    public function __construct(string $data) {
        $this->data = $data;
        
    }

    public function setNext(?Node $next) {
        $this->next = $next;
    }
}

class LinkedList
{
    private $head;

    public function insertFirst(Node $node) : void {
        $node->setNext($this->head);
        $this->head = $node;
    }
}

$linkedList = new LinkedList();
$linkedList->insertFirst(new Node('aaa'));
$linkedList->insertFirst(new Node('bbb'));
$linkedList->insertFirst(new Node('ccc'));

print_r($linkedList);