<?php
// Направи queue като използваш 2 stack-а. Да не се прави array в queue класа

class Stack implements Countable {
    private $stack = [];

    public function count() : int {
        return count($this->stack);
    }

    public function add(int $element) : void {
        $this->stack[] = $element;
    }

    public function getElement() : int {
        return array_pop($this->stack);
    }
}

class Queue implements Countable {
    private $queue = [];
    private $stack1;
    private $stack2;

    public function __construct() {
        $this->stack1 = new Stack();
        $this->stack2 = new Stack();
    }

    public function count() : int {
        return count($this->stack1);
    }

    public function add(int $element) : void {
        while(count($this->stack1) > 0) {
            $this->stack2->add($this->stack1->getElement());
        }

        $this->stack1->add($element);
        
        while(count($this->stack2) > 0) {
            $this->stack1->add($this->stack2->getElement());
        }
    }
     
    public function getElement() : int {
        return $this->stack1->getElement();
    }
}

$Queue = new Queue();
$Queue->add(1);
$Queue->add(2);
$Queue->add(3);

echo $Queue->getElement() . PHP_EOL;
echo $Queue->getElement() . PHP_EOL;
echo $Queue->getElement() . PHP_EOL;

