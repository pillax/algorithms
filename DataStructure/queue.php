<?php
// В опашката имаме FIFO First In First Out
// Демек влиза в поредност 1, 2, 3 и излиза в поредност 1, 2, 3 
// queue
Class Queue implements Countable {
    private $q = [];

    public function add($element) : void {
       array_unshift($this->q, $element);
    }

    public function remove() {
        return array_pop($this->q);
     }

    public function getQueue() : array {
        return $this->q;
    }

    public function count() : int {
        return count($this->q);
    }
}

$Queue1 = new Queue();
$Queue1->add(1);
$Queue1->add(2);

$Queue2 = new Queue();
$Queue2->add('Hi');
$Queue2->add('There');
$Queue2->add('One');
$Queue2->add('Two');

// Не трябва да се пипат елементите вътре в опашката
function mergeQueues(Queue $Queue1, Queue $Queue2) {
    $Queue3 = new Queue();

     while($Queue1->count() > 0 || $Queue2->count() > 0) {
        $q1Element = $Queue1->remove();
        if($q1Element) {
            $Queue3->add($q1Element);
        }

        $q2Element = $Queue2->remove();
        if($q2Element) {
            $Queue3->add($q2Element);
        }
     }

     return $Queue3;
}

$Queue3 = mergeQueues($Queue1, $Queue2);
echo $Queue3->remove() . PHP_EOL;
echo $Queue3->remove() . PHP_EOL;
echo $Queue3->remove() . PHP_EOL;
echo $Queue3->remove() . PHP_EOL;
echo $Queue3->remove() . PHP_EOL;
echo $Queue3->remove() . PHP_EOL;
