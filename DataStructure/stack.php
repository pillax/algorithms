<?php
// в Стак имаме FILO First In Last Out

class Stack implements Countable {
    private $stack = [];

    public function count() : int {
        return count($this->stack);
    }

    public function add(int $element) : void {
        $this->stack[] = $element;
    }

    public function getElement() : int {
        return array_pop($this->stack);
    }
}

$Stack = new Stack();
$Stack->add(1);
$Stack->add(2);
$Stack->add(3);
echo $Stack->getElement() . PHP_EOL;
echo $Stack->getElement() . PHP_EOL;
echo $Stack->getElement() . PHP_EOL;
