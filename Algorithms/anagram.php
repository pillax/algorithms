<?php

// Check 2 strings are using the same letters/integers + they are using same size of letters/integers

// isAnagram('Мария', 'армия') // true
// isAnagram('bosilek', 'obelisk!') // true
// isAnagram('varna', 'vra-na') // true
// isAnagram('opala', 'tralala') // false

function isAnagram2($str1, $str2) : bool {
    return strToJson($str1) === strToJson($str2);
}

function strToJson($str) : string {
    $arr = str_split($str);

    $out = [];
    foreach($arr as $v) {
        if((bool) preg_match('/[a-zA-z0-9]/', $v) === false) {
            continue;
        }

        $v = strtolower($v);

        if( ! isset($out[$v])) {
            $out[$v] = 0;
        }

        $out[$v]++;
    }

    ksort($out);

    return json_encode($out);
}


function isAnagram($str1, $str2) : bool {
    return prepareStr($str1) === prepareStr($str2);
}

function prepareStr($str) : string {
    $str = strtolower($str);
    $arr = str_split($str);
    $arr = array_filter($arr, function($v) { return (bool) preg_match('/[a-zA-z0-9]/', $v); });
    sort($arr);
    return implode('', $arr);   
}

echo isAnagram('Maria', 'armia') ? 'Y' : 'N';
echo isAnagram('opala', 'tralala') ? 'Y' : 'N';
echo isAnagram('bosilek', ' obe-lisk!') ? 'Y' : 'N';
echo isAnagram('RAIL! SAFETY!', 'fairy tales') ? 'Y' : 'N';
