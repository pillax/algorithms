<?php

$arr = [15, 981, 500, -15, -90];

foreach($arr as $v) {
    $multiplier = $v < 0 ? -1 : 1;

    $rev = strrev((string) $v);
    $rev = (int) $rev * $multiplier;

    echo $v . ' : ' . $rev . PHP_EOL;
}
