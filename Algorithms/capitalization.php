<?php

// Make each wotd in sentence capital
// isAnagram('opa tra la la') // Opa Tra La La

function capitalize($str) : string {
    $out = '';
    for($i=0; $i<strlen($str); $i++) {
        if($str[$i-1] == ' ' || $i==0) {
            $str[$i] = strtoupper($str[$i]);
        }

        $out .= $str[$i];
    }

    return $out;
}

function capitalize2($str) : string {
    $words = explode(' ', $str);
    $tmp = [];

    foreach($words AS $word) {
        $tmp[] = ucfirst($word);
    }

    return implode(' ', $tmp);
}

echo capitalize('opa tra la la') . PHP_EOL;
echo capitalize('togava, trygvame napred!') . PHP_EOL;
