<?php

// arrChunk([1,2,3,4,5], 2) -> [[1,2], [3,4], [5]]
// arrChunk([1,2,3,4,5], 20) -> [[1,2,3,4,5]]
    
function arrChunk(array $arr, int $chunkSize) {
    $out = [];
    $elementsUsed = 0;

    while($elementsUsed < count($arr)) {
        $out[] = array_slice($arr, $elementsUsed, $chunkSize);
        $elementsUsed += $chunkSize;
    }

    return $out;
} 

function arrChunk2(array $arr, int $chunkSize) {
    $out = [];

    $k = 0;
    foreach($arr as $v) {
        if(count($out[$k] ?? []) == $chunkSize) {
            $k++;
        }

        $out[$k][] = $v;
    }

    return $out;
} 

print_r(arrChunk2([1,2,3,4,5], 2));

