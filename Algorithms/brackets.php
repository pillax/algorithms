<?php
/*
Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Every close bracket has a corresponding open bracket of the same type.

Example 1:

Input: s = "()"
Output: true
Example 2:

Input: s = "()[]{}"
Output: true
Example 3:

Input: s = "(]"
Output: false
*/

function isValid($s) {
    $stack = new SplStack();
    $pairs = [
        '(' => ')',
        '[' => ']',
        '{' => '}',
    ];
    $opens = ['(', '[', '{'];
    for($i=0; $i<strlen($s); $i++) {
        $bracket = $s[$i];
        if(in_array($bracket, $opens)) {
            $stack->push($bracket);
        } else {
            if($bracket !== $pairs[$stack->pop()]) {
                return false;
            }
        }
    }

    return $stack->isEmpty();
}

echo (isValid('[[[]]]') ? 'y' : 'n') . PHP_EOL;