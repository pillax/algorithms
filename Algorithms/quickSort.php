<?php

function quickSort(array $arr) {

    // Save CPU time
    if(count($arr) < 2) {
        return $arr;
    }

    $left = [];
    $right = [];
    $pivotKey = key($arr);
    $pivotValue = array_shift($arr);

    foreach($arr as $v) {
        if((int) $v < $pivotValue) {
            $left[] = $v;
        } else {
            $right[] = $v;
        }
    }

    return array_merge(
        quickSort($left), 
        [$pivotKey => $pivotValue], 
        quickSort($right)
    );
}

print_r(quickSort([5, 3, 7, 1, 4, 4, 2]));


