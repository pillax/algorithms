<?php


$str = 'abcdef';

/////////////////////////////////////////////////////////
for($i=strlen($str)-1; $i >= 0; $i--) {
    echo $str[$i];
}
echo PHP_EOL;

/////////////////////////////////////////////////////////
$arr = str_split($str);
$arr = array_reverse($arr);
echo implode('', $arr);
echo PHP_EOL;

/////////////////////////////////////////////////////////
$rev = '';
foreach(str_split($str) as $v) {
    $rev = $v . $rev;
}
echo $rev;
echo PHP_EOL;

/////////////////////////////////////////////////////////
for($i=strlen($str); $i>=0; $i--) {
    echo substr($str, $i, 1);
}
echo PHP_EOL;

