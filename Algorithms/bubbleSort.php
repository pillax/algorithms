<?php
function bubbleSort(array $arr): array {
    $cnt = count($arr);
    do {
        $process = false;
        for ($i = 0; $i < $cnt-1; $i++) {
            if ($arr[$i] > $arr[$i + 1]) {
                $tmp = $arr[$i];
                $arr[$i] = $arr[$i + 1];
                $arr[$i + 1] = $tmp;
                $process = true;
            }
        }
    } while ($process);

    return $arr;
}


print_r(bubbleSort([2,1,5,3,2,2,8,0]));