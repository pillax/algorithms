<?php

$a = 'abcdxxdcba';

$arr = str_split($a);
print_r($arr);

$lastKey = count($arr)-1;
$isPalindrome = true;
for($i=0; $i<$lastKey; $i++) {
    if($arr[$i] !== $arr[$lastKey-$i]) {
        $isPalindrome = false;
    }
}

echo $isPalindrome ? 'Y' : 'N';