<?php

/**
 * Print $cnt elements from fibonachi numbers 
 * Example output: 1, 1, 2, 3, 5, 8, n...
 */
function fibonacchi(int $cnt) {
    $row = [];

    for($i=0; $i<$cnt; $i++) {
        if($i < 2) {
            $row[] = 1;
        } else {
            $row[] = $row[$i-2] + $row[$i-1];
        }
    }

    return $row;
}

print_r(fibonacchi(10));
echo PHP_EOL;

/////////////////////////////////////////////////////////

function fibonachiNext($n) {
    $result = [];

    if($n >= 1) {
        $result[] = 1;
    }

    if($n >= 2) {
        $result[] = 1;
    }

    for($i=2; $i<$n; $i++) {
        $result[] = $result[$i-2] + $result[$i-1];
    }

    return end($result);
}

echo fibonachiNext(10);
echo PHP_EOL;

/////////////////////////////////////////////////////////

function fibonachiNextRecursion($n) {
    if($n < 2) {
        return $n;
    }

    return fibonachiNextRecursion($n-2) + fibonachiNextRecursion($n-1);
}

echo fibonachiNextRecursion(10);
echo PHP_EOL;