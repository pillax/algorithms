<?php

$arr = [
    '1211121233443', 
    'asdAAssaassaad d', 
    'sdddaSsssssa a a'
];

foreach($arr as $v) {
    $tmp = [];
    foreach(str_split($v) AS $letter) {
        if( ! isset($tmp[$letter])) {
            $tmp[$letter] = 0;
        }

        $tmp[$letter]++; 
    }

    arsort($tmp);

    echo $v . ': ' . key($tmp) . PHP_EOL;
}