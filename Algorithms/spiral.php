<?php

// spiral(2)
// [
//     [1, 2]
//     [4, 3]
// ]
// spiral(3)
// [
//     [1, 2, 3]
//     [8, 9, 4]
//     [7, 6, 5]
// ]
// spiral(4)
// [
//     [1,  2,  3,  4]
//     [12, 13, 14, 5]
//     [11, 16, 15, 6]
//     [10, 9,  8,  7]
// ]


function spiral(int $side) : array {
    $out = [];

    // create empty matrix with side = $side
    for($row=0; $row < $side; $row++) {
        $out[] = array_fill(0, $side, 0);
    }

    $val = 1;
    $rowStart = 0;
    $colStart = 0;
    $rowEnd = $side - 1;
    $colEnd = $side - 1;

    while($rowStart <= $rowEnd && $colStart <= $colEnd) {

        // top row
        for($i=$colStart; $i<=$colEnd; $i++) {
            $out[$rowStart][$i] = $val;
            $val++;
        }
        $rowStart++;

        // right coll
        for($i=$rowStart; $i<=$rowEnd; $i++) {
            $out[$i][$colEnd] = $val;
            $val++;
        }
        $colEnd--;
       
        // bottom row
        for($i=$colEnd; $i>=$colStart; $i--) {
            $out[$rowEnd][$i] = $val;
            $val++;
        }
        $rowEnd--;

        // left coll
        for($i=$rowEnd; $i>=$rowStart; $i--) {
            $out[$i][$colStart] = $val;
            $val++;
        }
        $colStart++;
    }

    return $out;
}

print_r(spiral(4));
