<?php

// pyramid(3)
// --#--
// -###-
// #####
function pyramid(int $x) : void {
    $collsCnt = ($x * 2 - 1);
    $middle = floor($collsCnt / 2);
    
    for($row=0; $row<$x; $row++) { 
        for($coll=0; $coll<$collsCnt; $coll++) { 
            if($middle - $row <= $coll && $middle + $row >= $coll) {
                echo '#';
            } else {
                echo '-';
            }
            
        }
        echo PHP_EOL;
    }
}

pyramid(3);




// recursion
function pyramidr(int $levels, $row=0, $coll=0) : void {
    if($row == $levels) {
        return;
    }

    $collsCnt = ($levels * 2 - 1);
    $middle = floor($collsCnt / 2);
    
    if($middle - $row <= $coll && $middle + $row >= $coll) {
        echo '#';
    } else {
        echo '-';
    }
    $coll++;

    if($coll == $collsCnt) {
        $row++;
        $coll=0;
        echo PHP_EOL;
    }

    pyramidr($levels, $row, $coll);
}

pyramidr(3);

// p(3)
// 0->1: -- # --    2x1-1
// 1->2: -# # #-    2x2-1
// 2->3: ## # ##    2x3-1

