<?php

// Make each wotd in sentence capital
// steps(3)
// #--
// ##-
// ###
function steps2(int $x) : void {
    for($i=1; $i<=$x; $i++) {
        echo str_repeat('#', $i) . str_repeat('-', $x-$i) . PHP_EOL;
    }
}


function steps(int $x) : void {
    for($i=1; $i<=$x; $i++) {
        for($j=1; $j<=$x; $j++) {
            if($j <= $i) {
              echo '#';
            } else {
              echo '-';
            }
        }
        echo PHP_EOL;
    }
}

steps(5);